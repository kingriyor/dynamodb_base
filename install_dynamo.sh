#  REPLACE "/home/kingriyor" to your own home dir
sudo apt-get install openjdk-7-jre-headless -y

cd /home/kingriyor/
mkdir -p dynamodb && cd dynamodb

wget http://dynamodb-local.s3-website-us-west-2.amazonaws.com/dynamodb_local_latest.tar.gz
tar -xvzf dynamodb_local_latest.tar.gz
# mv dynamodb_local_latest/ dynamodb/

cat >> dynamodb.conf << EOF
description "DynamoDB Local"

start on (local-filesystems and runlevel [2345])
stop on runlevel [016]

chdir /home/kingriyor/dynamodb/dynamodb

setuid ubuntu
setgid ubuntu

exec java -Djava.library.path=. -jar DynamoDBLocal.jar
EOF
sudo cp /home/kingriyor/dynamodb/dynamodb.conf /etc/init/dynamodb.conf
