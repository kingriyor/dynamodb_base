# Installing the AWS CLI with Pip
$ pip install awscli --upgrade --user

# Get credentials from ur aws account which will be used in configuration

# configure awscli
$ aws configure

# Restart your machine

# install dynamo locally with sh script

# go to dynamo folder containing "DynamoDBLocal.jar" and run
$ java -Djava.library.path=./DynamoDBLocal_lib/ -jar DynamoDBLocal.jar

# while its still running on terminal go to browser
http://localhost:8000/shell/


