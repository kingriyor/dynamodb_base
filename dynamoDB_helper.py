import boto3

# Get the service resource.
dynamodb = boto3.resource('dynamodb') # connect to aws dynamo
# dynamodb = boto3.resource('dynamodb', endpoint_url='http://localhost:8000') # connect to local dynamo

boto_key = boto3.dynamodb.conditions.Key 
boto_attr = boto3.dynamodb.conditions.Attr

class dynamodb_helper:
    def create_table(table_name, keySchema_body, attributeDefinitions_body):
        # Create the DynamoDB table.
        '''
        keySchema_body = [
            {
                'AttributeName': 'username',
                'KeyType': 'HASH'
            },
            {
                'AttributeName': 'last_name',
                'KeyType': 'RANGE'
            }
        ]
        '''

        '''
        attributeDefinitions_body = [
            {
                'AttributeName': 'username',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'last_name',
                'AttributeType': 'S'
            },

        ]
        '''
        table = dynamodb.create_table(
            TableName= table_name,
            KeySchema= keySchema_body,
            AttributeDefinitions= attributeDefinitions_body,
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )

        # Wait until the table exists.
        table.meta.client.get_waiter('table_exists').wait(TableName=table_name)

        # Print out some data about the table.
        print(table.item_count)

    def delete_table(table_name):
        table = dynamodb.Table(table_name)
        table.delete()
        print("TABLE DELETED")
        return "TABLE DELETED"
        
    def add_doc(table_name, item_body):
        ''' item_body={
                'username': 'janedoe',
                'first_name': 'Jane',
                'last_name': 'Doe',
                'age': 25,
                'account_type': 'standard_user',
            }
        '''
        table = dynamodb.Table(table_name)
        table.put_item(Item=item_body)
        print("Added")
        return item_body

    def get_doc(table_name,key_id):
        '''
        key_id={
                'username': 'janedoe',
                'last_name': 'Doe'
            }
        '''
        table = dynamodb.Table(table_name)
        response = table.get_item(Key=key_id)
        item = response['Item']
        print(item)
        return item

    def update_doc(table_name, key_id, UpdateExpression_key, UpdateExpression_value):
        table = dynamodb.Table(table_name)
        '''
        key_id={
                'username': 'janedoe',
                'last_name': 'Doe'
            }

        UpdateExpression_key = 'age'

        UpdateExpression_value = 26

        '''
        

        UpdateExpression_body = 'SET '+UpdateExpression_key+' = :val1'
        
        
        ExpressionAttributeValues_body = {
                ':val1': UpdateExpression_value
            }
        

        table.update_item(
            Key=key_id,
            UpdateExpression= UpdateExpression_body,
            ExpressionAttributeValues= ExpressionAttributeValues_body
        )
        print("UPDATED")

        # Then get back updated value
        response = table.get_item(Key=key_id)
        item = response['Item']
        print(item)
        return item

    def delete_doc(table_name, key_id):
        table = dynamodb.Table(table_name)
        '''
        key_id={
                'username': 'janedoe',
                'last_name': 'Doe'
            }
        '''
        table.delete_item(
            Key= key_id
        )
        print("DELETED")
        return "DELETED"

    def get_doc_by_query(table_name, key_name, value):
        # here the key doesn't need tp be a primary key
        '''
        key_name='username'
        value='johndoe'
        '''
        table = dynamodb.Table(table_name)
        response = table.query(
            KeyConditionExpression=boto_key(key_name).eq(value)
        )
        items = response['Items']
        print(items)
        return items
                
    def getall_docs(table_name):
        table = dynamodb.Table(table_name)
        response = table.scan()
        items = response['Items']
        print(items)
        return items     

    # BATCH ADD DOCS  -  NOT IN USE YET
    def batch_write(table_name):
        # add multiple items at the ame time
        table = dynamodb.Table(table_name)

        # OPTION A
        with table.batch_writer() as batch:
            for i in range(10):
                batch.put_item(
                    Item={
                        'account_type': 'anonymous',
                        'username': 'user' + str(i),
                        'first_name': 'unknown',
                        'last_name': 'unknown'
                    }
                )

        # OPTION B
        # with table.batch_writer() as batch:
        #     batch.put_item(
        #         Item={
        #             'account_type': 'standard_user',
        #             'username': 'johndoe',
        #             'first_name': 'John',
        #             'last_name': 'Doe',
        #             'age': 25,
        #             'address': {
        #                 'road': '1 Jefferson Street',
        #                 'city': 'Los Angeles',
        #                 'state': 'CA',
        #                 'zipcode': 90001
        #             }
        #         }
        #     )
        #     batch.put_item(
        #         Item={
        #             'account_type': 'super_user',
        #             'username': 'janedoering',
        #             'first_name': 'Jane',
        #             'last_name': 'Doering',
        #             'age': 40,
        #             'address': {
        #                 'road': '2 Washington Avenue',
        #                 'city': 'Seattle',
        #                 'state': 'WA',
        #                 'zipcode': 98109
        #             }
        #         }
        #     )
        #     batch.put_item(
        #         Item={
        #             'account_type': 'standard_user',
        #             'username': 'bobsmith',
        #             'first_name': 'Bob',
        #             'last_name':  'Smith',
        #             'age': 18,
        #             'address': {
        #                 'road': '3 Madison Lane',
        #                 'city': 'Louisville',
        #                 'state': 'KY',
        #                 'zipcode': 40213
        #             }
        #         }
        #     )
        #     batch.put_item(
        #         Item={
        #             'account_type': 'super_user',
        #             'username': 'alicedoe',
        #             'first_name': 'Alice',
        #             'last_name': 'Doe',
        #             'age': 27,
        #             'address': {
        #                 'road': '1 Jefferson Street',
        #                 'city': 'Los Angeles',
        #                 'state': 'CA',
        #                 'zipcode': 90001
        #             }
        #         }
        #     )

    # ATTRIBUTE SEARCH  - NOT IN USE YET
    def get_docs_by_search_coditions(table_name):
        table = dynamodb.Table(table_name)

        # EXAMPLE 1 - this scans for all the users whose age is less than 27
        response = table.scan(
            FilterExpression=Attr('age').lt(27)
        )
        items = response['Items']
        print(items)

        # EXAMPLE 2 - this scans for all users whose first_name starts with J and whose account_type is super_user
        response = table.scan(
            FilterExpression=Attr('first_name').begins_with('J') & Attr('account_type').eq('super_user')
        )
        items = response['Items']
        print(items)

        # EXAMPLE 3 - this scans for all users whose state in their address is CA
        response = table.scan(
            FilterExpression=Attr('address.state').eq('CA')
        )
        items = response['Items']
        print(items)
                
        
# -----------------------------------TESTING-------------------------------------

table_name = 'ariyo_test'            
                
keySchema_body = [
            {
                'AttributeName': 'username',
                'KeyType': 'HASH'
            },
            {
                'AttributeName': 'last_name',
                'KeyType': 'RANGE'
            }
        ]


attributeDefinitions_body = [
            {
                'AttributeName': 'username',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'last_name',
                'AttributeType': 'S'
            },

        ]

item_body={
                'username': 'kingriyor',
                'first_name': 'john',
                'last_name': 'Doe',
                'age': 25,
                'account_type': 'standard_user',
            }

key_id={
                'username': 'janedoe',
                'last_name': 'Doe'
            }

UpdateExpression_key = 'age'
UpdateExpression_value = 30
key_name='username'
value='janedoe'

# CREATE TABLE
# dynamodb_helper.create_table(table_name, keySchema_body, attributeDefinitions_body)


# ADD SINGLE DOC
# dynamodb_helper.add_doc(table_name, item_body)

# BATCH ADD DOCS
# dynamodb_helper.batch_write(table_name)

# GETALL FROM TABLE
dynamodb_helper.getall_docs(table_name)

# UPDATE DOC
# dynamodb_helper.update_doc(table_name, key_id, UpdateExpression_key, UpdateExpression_value)

# DELETE TABLE
# dynamodb_helper.delete_table(table_name)